// text
const pengeluaran = "pengeluaran";
const pemasukan = "pemasukan";

// key
const moneyReportKey = "moneyreports";

// selector
const loadMoreID = "load-more";
const reportsWrapperID = "reports";
const addReportButtonID = "add-report";
const reportItemPartID = "report-item-";
const reportFormID = "report-form";
const totalPemasukanID = "total-pemasukan";
const totalPengeluaranID = "total-pengeluaran";
const sisaID = "sisa";

// element
const loadMoreElement = document.getElementById(loadMoreID);
const totalPemasukanElement = document.getElementById(totalPemasukanID);
const totalPengeluaranElement = document.getElementById(totalPengeluaranID);
const sisaElement = document.getElementById(sisaID);
const reportFormElement = document.getElementById(reportFormID);
const reportWrapperElement = document.getElementById(reportsWrapperID);
const addReportButtonElement = document.getElementById(addReportButtonID);

// data

const months = [
  "Januari",
  "Februari",
  "Maret",
  "April",
  "Mei",
  "Juni",
  "Juli",
  "Agustus",
  "September",
  "Oktober",
  "November",
  "Desember"
];

const initData = [
  {
    id: 1,
    date: "10 Agustus 2020",
    title: "Ngasdos",
    amount: 550000,
    category: pemasukan
  },
  {
    id: 2,
    date: "11 Agustus 2020",
    title: "Beli Dicoding Voucher",
    amount: 350000,
    category: pengeluaran
  },
  {
    id: 3,
    date: "12 Agustus 2020",
    title: "Saham",
    amount: 3500000,
    category: pengeluaran
  },
  {
    id: 4,
    date: "13 Agustus 2020",
    title: "Penjualan Web",
    amount: 15000000,
    category: pemasukan
  },
  {
    id: 5,
    date: "14 Agustus 2020",
    title: "Nonton bareng pacar",
    amount: 10000,
    category: pengeluaran
  },
  {
    id: 6,
    date: "15 Agustus 2020",
    title: "Seminar Javascript",
    amount: 100000,
    category: pengeluaran
  },
  {
    id: 7,
    date: "16 Agustus 2020",
    title: "Penjualan Web",
    amount: 15000000,
    category: pemasukan
  },
  {
    id: 8,
    date: "17 Agustus 2020",
    title: "Makan bareng pacar",
    amount: 20000,
    category: pengeluaran
  },
  {
    id: 9,
    date: "18 Agustus 2020",
    title: "Seminar Web",
    amount: 100000,
    category: pengeluaran
  },

  {
    id: 10,
    date: "19 Agustus 2020",
    title: "Liburan ke pantai",
    amount: 10000,
    category: pengeluaran
  },
  {
    id: 11,
    date: "20 Agustus 2020",
    title: "Beli Monitor",
    amount: 3000000,
    category: pengeluaran
  }
];
