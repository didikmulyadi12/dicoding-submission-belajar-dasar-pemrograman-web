function showReports(start = 0, end = 5) {
  let reportsHTML = "";
  getReports()
    .sort((a, b) => b.id - a.id)
    .slice(start, end)
    .forEach(report => {
      reportsHTML += getReportItemHTML(report);
    });
  reportWrapperElement.innerHTML = reportsHTML;
}

function showHideLoadMore() {
  const reportsLength = reportWrapperElement.children.length;
  const totalReports = getReports().length;

  if (reportsLength < totalReports) {
    loadMoreElement.style.display = "block";
  } else {
    loadMoreElement.style.display = "none";
  }
}

function recalculateSummary() {
  let totalPemasukan = 0;
  let totalPengeluaran = 0;
  getReports().forEach(({ amount, category }) => {
    category === "pengeluaran"
      ? (totalPengeluaran += amount)
      : (totalPemasukan += amount);
  });

  const sisa = totalPemasukan - totalPengeluaran;
  totalPemasukanElement.innerText = toRupiah(totalPemasukan);
  totalPengeluaranElement.innerText = toRupiah(totalPengeluaran);
  sisaElement.classList.remove("negative");
  sisaElement.classList.remove("positive");
  sisaElement.classList.add(sisa < 0 ? "negative" : "positive");
  sisaElement.innerText =
    sisa < 0 ? `- ${toRupiah(Math.abs(sisa))}` : toRupiah(sisa);
}
