const getReports = () => JSON.parse(localStorage.getItem(moneyReportKey)) || [];

const getLatestID = () => getReports().sort((a, b) => b.id - a.id)[0].id;

const getDateFormatted = date => {
  const dateObj = new Date(date);

  return `${dateObj.getDate()} ${
    months[dateObj.getMonth()]
  } ${dateObj.getFullYear()}`;
};

const toRupiah = amount => {
  const amountString = amount.toString();

  const mod = amountString.length % 3;
  const rupiah = amountString.substr(0, mod);
  const ribuan = amountString.substr(mod).match(/\d{3}/gi);

  if (ribuan) {
    return `Rp. ${rupiah}${ribuan && mod ? "." : ""}${ribuan.join(".")}`;
  }

  return `Rp. ${rupiah}`;
};

const getReportItemHTML = ({ id, title, amount, date, category }) => `
<article class="box bg-shadow report-item" id="${reportItemPartID + id}">
    <header class="v-center h-between">
        <h4>${date}</h4>
        <button type="button" onclick="onRemoveReport(this);" data-id="${id}" class="danger bg-shadow">Hapus</button>
    </header>
    <section class="v-center h-between">
        <h3>${title}</h3>
        <h3 class="${category}">${ category === "pemasukan" ? "+" : "-" } ${toRupiah(amount)}</h3>
    </section>
</article>
`;
