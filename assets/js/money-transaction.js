function initialization() {
  if (typeof Storage !== "undefined") {
    !localStorage.getItem(moneyReportKey) &&
      localStorage.setItem(moneyReportKey, JSON.stringify(initData));

    showReports();
    recalculateSummary();
    showHideLoadMore();
  } else {
    alert("Browser Anda tidak mendukung web storage");
  }
}

function onRemoveReport(el) {
  const deletedID = el.getAttribute("data-id");
  const reportItem = document.getElementById(reportItemPartID + deletedID);

  localStorage.setItem(
    moneyReportKey,
    JSON.stringify(getReports().filter(val => val.id !== parseInt(deletedID)))
  );

  reportItem.parentNode.removeChild(reportItem);
  recalculateSummary();
  showHideLoadMore();
}

function cancelAddReport() {
  reportFormElement.reset();
  addReportButtonElement.style.display = "block";
  reportFormElement.style.display = "none";
}

reportFormElement.addEventListener("submit", function(e) {
  e.preventDefault();
  const { fdate, ftitle, famount, fcategory } = e.target;
  const reportsLength = reportWrapperElement.children.length;

  localStorage.setItem(
    moneyReportKey,
    JSON.stringify([
      ...getReports(),
      {
        id: getLatestID() + 1,
        date: getDateFormatted(fdate.value),
        title: ftitle.value,
        amount: parseInt(famount.value),
        category: fcategory.value
      }
    ])
  );

  showReports(0, reportsLength + 1);
  cancelAddReport();
  recalculateSummary();
});

addReportButtonElement.addEventListener("click", function() {
  addReportButtonElement.style.display = "none";
  reportFormElement.style.display = "block";
});

loadMoreElement.addEventListener("click", function() {
  const totalReports = getReports().length;
  const reportsLength = reportWrapperElement.children.length;

  showReports(0, reportsLength + 5);
  showHideLoadMore();
});

initialization();
